#!/usr/bin/env python3

i=5
print(i)
i=i+1
print(i)

s='''This is a multiline string.
This is the second line.'''
print(s)

t="this is a string\
This line continues."

print(t)

k="aaaa"
print\ 
(k)
#то же самое, что и

print(k)
