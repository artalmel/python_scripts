#!/usr/bin/python3

a="123456789"
print("a=",a)

print("k=[3:7]")
k=a[3:7]
print(k)

print("k=a[3:7:2]")
k=a[3:7:2] 
print(k)

print("k=a[3:-2]")
k=a[3:-2] 
print(k)

print("k=a.find('345')")
k=a.find("345")
print(k)

print("k=a.rfind('345')")
k=a.rfind("345")
print(k)

print("k=a.replace('345','AAA')")
k=a.replace('345','AAA')
print(k)

print("k=a.split('4')")
k=a.split('4')
print(k)

print("a.isdigit()")
k=a.isdigit()
print(k)

a="abcdefg"
print("a=",a)

print("k=a.isalpha()")
k=a.isalpha()
print(k)

print("k=a.isupper()")
k=a.isupper()
print(k)

print("k=a.islower()")
k=a.islower()
print(k)

print("k=a.upper()")
k=a.upper()
print(k)

print("k=a.lower()")
k=a.lower()
print(k)


